# Babelbox Packer build

This repository provides build configuration and Debian installer preseed files
for building [Babelbox](https://wiki.debian.org/DebianInstaller/BabelBox)
virtual machine images for QEMU/KVM and VirtualBox using [HashiCorp
Packer](https://packer.io/).

## Building the VM images

The images need to be built in separate steps because VirtualBox and KVM cannot
reserve the needed CPU extension (VT-x, AMD-V) at the same time.

```
packer build --only=qemu debian-babelbox.json
packer build --only=virtualbox-iso debian-babelbox.json
```

## Preparing for Babelbox demo

Due to image size reasons we do not include a Debian DVD ISO image into the
image. Therefore the VMs have to be booted into the regular OS (first Grub menu
option). Login as user `demo` with password `babelbox` and download a Debian
DVD ISO image into `/srv/`:

```
cd /srv
sudo wget https://cdimage.debian.org/debian-cd/10.3.0/amd64/iso-dvd/debian-10.3.0-amd64-DVD-1.iso
```

The version might be different but 10.3.0 has been used for my tests.
