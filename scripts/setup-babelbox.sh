#!/bin/sh

set -e

apt-get install -y git wget
cd /srv
wget http://ftp.debian.org/debian/dists/stable/main/installer-amd64/current/images/hd-media/gtk/initrd.gz http://ftp.debian.org/debian/dists/stable/main/installer-amd64/current/images/hd-media/gtk/vmlinuz http://ftp.debian.org/debian/pool/main/u/util-linux/fdisk-udeb_2.33.1-0.1_amd64.udeb
mkdir initrd_with_fdisk
cd initrd_with_fdisk
gunzip -c ../initrd.gz | cpio -id
dpkg-deb -x ../fdisk-udeb_2.33.1-0.1_amd64.udeb .
find . | cpio --create --format='newc' | gzip -9 > ../initrd.gz
cd ..
rm -rf initrd_with_fdisk fdisk-udeb_2.33.1-0.1_amd64.udeb
git clone https://salsa.debian.org/installer-team/babelbox.git
cd babelbox
cat babelbox-grub >> /etc/grub.d/40_custom
update-grub
./cronscript

echo "You need to download a Debian amd64 DVD ISO into /srv"
