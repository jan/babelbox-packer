#!/bin/sh

set -e

case "$PACKER_BUILDER_TYPE" in
  virtualbox-iso)
    DEVICE=/dev/sda
    ;;
  qemu)
    DEVICE=/dev/vda
    ;;
  *)
    echo "Unsupported packer build $PACKER_BUILDER_TYPE" >&2
    exit 1
esac

echo "
d
3
n
e
3

+3G
n
p


w" | /sbin/fdisk "$DEVICE" || true
/sbin/partprobe
