# Clean up

apt-get --yes remove dmidecode gcc-8 laptop-detect libc6-dev linux-libc-dev
apt-get --yes autoremove
apt-get --yes clean

# Removing leftover leases and persistent rules
echo "cleaning up dhcp leases"
rm /var/lib/dhcp/*

echo "Adding a 2 sec delay to the interface up, to make the dhclient happy"
echo "pre-up sleep 2" >> /etc/network/interfaces
